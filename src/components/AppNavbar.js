
import {NavDropdown,Nav,Navbar,Container} from 'react-bootstrap'
import {useContext} from 'react'
import {Link, NavLink, Outlet} from 'react-router-dom'
import UserContext from '../UserContext.js'
import { ImUserPlus,ImCart,ImHome,ImUser,ImGlass } from "react-icons/im";
import {RiAdminFill,RiLogoutBoxRFill,RiAccountCircleFill} from "react-icons/ri";

export default function AppNavbar(){
	const {user, setUser} = useContext(UserContext);

	console.log(typeof user.isAdmin)
	return(
	<Container fluid className="px-0">
		<Navbar expand="lg" className="conpt-2 px-2 pb-1  mb-0 " >
			<Navbar.Brand as={NavLink} exact to="/">
			<img
              src="https://w7.pngwing.com/pngs/127/887/png-transparent-fizzy-drinks-juice-sprite-coca-cola-fanta-soft-drinks-plastic-bottle-drinking-soft-drinks-thumbnail.png"
              width="80"
              height="30"

              className="px-3"
         
            />
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id ="basic-navbar-nav">
			<div className="navm-right px-2" >
				<Nav className=" nav-menu ">

					{	

						(user.isAdmin!== null && user.isAdmin !== false)?
						<>	
							<ImHome size={20}/>
							<Nav.Link  className="nav" as={NavLink} exact to="/">Home</Nav.Link>
							<ImGlass size={20}/>
							<Nav.Link  className="nav" as={NavLink} exact to="/products">Products</Nav.Link>
							<RiAdminFill size={20}/>
							<Nav.Link className="nav" as={NavLink} exact to="/admin">AdminDashBoard</Nav.Link>
							<RiLogoutBoxRFill size={20}/>
							<Nav.Link className="nav float-right" as={NavLink} exact to="/logout">Logout</Nav.Link>
						</>
						:(user.token !== null && user.isAdmin !== true)?
							<>
							<ImHome size={50}/>
							<Nav.Link  className="nav" as={NavLink} exact to="/">Home</Nav.Link>
							<ImGlass size={50}/>
							<Nav.Link  className="nav" as={NavLink} exact to="/products">Products</Nav.Link>
							<ImCart size={50}/>
							<Nav.Link >Cart</Nav.Link>

							<RiAccountCircleFill size={50}/>
							<Container fluid>
								<NavDropdown title="Account" id="basic-nav-dropdown">

						              <NavDropdown.Item as={NavLink} exact to="/user/profile">My Profile</NavDropdown.Item>
						              <NavDropdown.Item as={NavLink} exact to="/logout">Logout</NavDropdown.Item>
								</NavDropdown>
							</Container>
							</>
						:
						<>
							<ImHome size={20}/>
							<Nav.Link  className="nav" as={NavLink} exact to="/">Home</Nav.Link>
							<ImGlass size={20}/>
							<Nav.Link  className="nav" as={NavLink} exact to="/products">Products</Nav.Link>
							<ImUser size={20}/>
							<Nav.Link className="nav" as={NavLink} exact to="/login">Login</Nav.Link>
							<ImUserPlus size={20}/>
							<Nav.Link className="nav" as={NavLink} exact to="/register">Register</Nav.Link>
						</>
						
					
					}
				</Nav>
			</div>
			</Navbar.Collapse>
		
		</Navbar>
	</Container>


		)
}