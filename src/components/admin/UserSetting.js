import UserContext from '../../UserContext.js'
//import PropagateLoader from 'react-spinners/PropagateLoader'
import {Table,Container,NavDropdown,Nav,Navbar,DropdownButton,Dropdown} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';


export default function UserSetting() {

	const {user, setUser} = useContext(UserContext);
	
	const [users, setUsers] = useState([])


	const fetchUsers = async () => {
	
		const response = await fetch(`${process.env.REACT_APP_API_URL }/users/allUsers`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})
		const data = await response.json()
		
		setUsers(data)
		
	}
	console.log(users);
	useEffect(() => {
		fetchUsers()
	}, [])


	return(
		<div className="admin-bg p-0">
			<div className="container table-container">							
			<div className="w-100 text-white mb-3 admin-titles">
				<h3>Users List</h3>
			</div>
			
				<Table striped bordered hover variant="black">
			      <thead>
			        <tr>
			          <th>Email</th>
			          <th>Name</th>
			          <th>UserID</th>
			          <th>isAdmin</th>
			        </tr>
			      </thead>
			      <tbody>
			        
			        	<tr >			        		
			        		<td>sa</td>
			        		<td>ds</td>
			        		<td>ds</td>
			        		<td></td>
			        	</tr>	        	
			   
			      </tbody>
			    </Table>
					
			
			</div>
		</div>
		)
}
