import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col,Image,InputGroup, FormControl, ListGroup,Form } from 'react-bootstrap';
import { BiMinusCircle, BiPlusCircle } from 'react-icons/bi'
import {useParams, useNavigate, Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal';
import UserContext from '../../UserContext.js'
import Swal from 'sweetalert2'

export default function ProductView() {

	const [showOrder, setShowOrder] = useState(false);
    const handleCloseOrder = () => setShowOrder(false);
			
			
	const navigate = useNavigate();
	const { user } =useContext(UserContext);
	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [brand, setBrand] = useState("");
	const [price, setPrice] =useState(0);
	const [stock, setStock] =useState("");
	const [count , setCount] = useState(1)
	const total= price * count
	useEffect(() =>{
		
		fetch(`${process.env.REACT_APP_API_URL }/products/${productId}`)
				.then(res => res.json())
				.then(data => {

			

					setName(data.productName);
					setDescription(data.description);
					setBrand(data.productBrand);
					setPrice(data.price);
					setStock(data.stock);

				});
	}, [])
	
 function orderProduct(event) {
            event.preventDefault();
      
            fetch(`${process.env.REACT_APP_API_URL }/users/myOrder`,{
                method: 'POST',
                headers: {
                		Authorization: `access ${user.token}`,
                        'Content-Type': 'application/json; charset=UTF-8'
                 },
                body: JSON.stringify({
                    productId: `${productId}`,
                    productName: name,
                    productBrand: brand,
                    price: price,
                    quantity: count,
                    totalAmount: total
                })
            },[])
                Swal.fire({

                            title: "Product Order Successfully",
                            icon: "success"
                })
                navigate("/products")	
                          

    }
	
	return(
		<>
		<Container className="mt-5 col-lg-9 con-userprofile ">
		<div className="bg-itempage">
			<div className="container d-flex justify-content-between align-items-center">
				<div className="image-container-item">
					<Image width="500px" src="https://place-puppy.com/450x450" fluid />
				</div>
				<div className="product-details px-5 col-lg-5">
					<div >
						<h1 >{name}</h1>
						<p>{description}</p>
						<h4>Php {price}</h4>
						<h5>Stock: {stock}</h5>
					</div>
					{
						(user.id !== null)?
					<>	
					<div className="mt-4 d-flex align-items-center count-buttons">
								<BiPlusCircle onClick={() => setCount(count + 1)} className="plus-count" size={40}/>			
								{count}
								<BiMinusCircle onClick={() => count > 1 && 	setCount(count - 1)} className="plus-count" size={40}/>	
							</div>
					<Button  variant="danger" onClick={() => setShowOrder(true)}>
						Buy Now
					</Button>
					</>
						:
						<Link class="btn btn-danger btn-block" to="/login">Login</Link>

					}			
					
				</div>
			</div>
		</div>
		</Container>

		 <Modal
                show={showOrder}
                onHide={() => setShowOrder(false)}
                dialogClassName="modal-90w"
                aria-labelledby="lol"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="lol">
                    Order
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form  className="mt-4" onSubmit={event=> {orderProduct(event) } }>
                         <Form.Group className="mb-3" controlId="productName">
                            <Form.Label hidden>`${productId}`</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>{name}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="productName">
                            <Form.Label>Product Brand: {brand}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description:  {description}</Form.Label>   
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Quantity: {count}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Total: ₱{total}</Form.Label>
                        </Form.Group>  
                        <Button variant="danger" type="submit" controlId="submitBtn" onClick={handleCloseOrder}>Order</Button>
                    </Form>
                </Modal.Body>
              </Modal>
             </>
		)
}