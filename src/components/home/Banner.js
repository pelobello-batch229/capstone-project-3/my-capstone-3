
import {Button, Row, Col, Container,Image,Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'
export default function Banner() {
	return(
		<>
	
	<Container >
		<Image className="mx-auto" width="500px" src="https://cdn.punchng.com/wp-content/uploads/2017/03/29201341/soft-drinks.png"  />
		
		<Row >
			<Col className="p-5 text-center">
			
				<h1>Welcome to My Store</h1>
				<h3>BUY! BUY! BUY!</h3>

				<Button variant="warning" ><Nav.Link  as={NavLink} exact to="/products">LETS GOOOOOOOOOOO!!!</Nav.Link></Button>
				

			</Col>
		</Row>
	</Container>	
	</>
		)
}