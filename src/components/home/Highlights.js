import {Row,Col, Card,Button,CardGroup, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link} from 'react-router-dom'

export default function Highlights({productProps}){

     const {productName,productBrand,description, price, _id, isActive} = productProps;
     let title1=productName;
      const [expanded, setExpanded] = useState(false);
	return(

   <div className="card-container col-lg-12 col-md-12 ">
      <Card className="cardHighlight m-3 ">
        <Card.Body className="d-flex flex-column card-body">
          <Card.Img variant="top" width="100px" src="https://www.coca-cola.co.uk/content/dam/one/gb/en/product/sprite-pet-500-1.png" />
          <div>
            <Card.Title className="pb-2">{productName}</Card.Title>
            <Card.Subtitle>Php {price}</Card.Subtitle>
            <br/>
            <Card.Subtitle className="pb-2">Description:
               <span onClick={() => setExpanded(!expanded)}>View more</span>
          {expanded && (
            <Card.Text className="accodion">{description}</Card.Text>
             )}
            </Card.Subtitle>
           
          </div>
            <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
            
          
        </Card.Body>
      </Card>
    </div>



		)
}