import AllProducts from '../components/admin/DashBoard.js'
import UserSetting from '../components/admin/UserSetting.js'

import {useState, useEffect ,useContext} from 'react';
import UserContext from '../UserContext.js'
import {NavLink,Navigate} from 'react-router-dom'
import {Nav,Button,Container} from 'react-bootstrap'
export default function Admin(){

	const {user} = useContext(UserContext);

	const [products, setProducts] = useState([]);
	useEffect(() =>{
    	fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	        .then(res => res.json())
        	.then(data => {
	            console.log(data);
	            setProducts(data.map(products => {
	            	return(
	            		<AllProducts key={products.id} productProps={products}/>
	            		)
	            }))
	        });
    }, [])


    return (

    	<>
    	<Container className="pt-">
    	{/*<UserSetting/>*/}
    	</Container>
    		<h1 className="text-center">Admin DashBoard</h1>
    		<div className="text-center">
    		     <Button  className="text-center"variant="danger" type="button">
    			<Nav.Link  as={NavLink} exact to="/admin/addproduct">Add New Product</Nav.Link>
    		</Button>
    		 <Button  className="text-center mx-3"variant="danger" type="button">
    			<Nav.Link  as={NavLink} exact to="/admin/usersettings">User List</Nav.Link>
    		</Button>
    		
    		</div>

    		
    		{products}
    		

    	</>

    	)

	
}