import UserContext from '../UserContext.js'
//import PropagateLoader from 'react-spinners/PropagateLoader'
import {Table,Container,NavDropdown,Nav,Navbar,DropdownButton,Dropdown} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

import {Form, Button,Col,Row} from 'react-bootstrap';
export default function Profile() {

	const {user, setUser} = useContext(UserContext);
	
	const [orders, setOrders] = useState([])


	const fetchOrders = async () => {
	
		const response = await fetch(`${process.env.REACT_APP_API_URL }/users/${user.id}/allMyOrder`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})
		const data = await response.json()
		
		setOrders(data)
		
	}
	console.log(orders);
	useEffect(() => {
		fetchOrders()
	}, [])
	

	return(
	<Container className="col-lg-7 con-userprofile">	
		<div className="admin-bg">
			<div className="container table-container">							
			<div className="w-100 text-white mb-3 admin-titles">
				>
			</div>
				<h1>Ordered Products</h1>
				<Table striped bordered hover variant="warning">

			      <thead  className="dark">
			        <tr>
			          <th>ProductName</th>
			          <th>Price</th>
			          <th>Quantity</th>
			          <th>Total Amount</th>
			        </tr>
			      </thead>
			      <tbody>
			        {orders.map((prod) => (
			        	<tr key={prod._id}>	
			        	<td>{prod.productName}</td>
			        	<td>{prod.price}</td>
			        	<td>{prod.quantity}</td>
			        	<td>{prod.totalAmount}</td>
			        	</tr>	        	
			        ))}  	
			   
			      </tbody>
			    </Table>
					
			
			</div>
		</div>
	</Container>	
		)
}