import Banner from '../components/home/Banner.js'
import Highlights from '../components/home/Highlights.js'
import UserContext from '../UserContext.js'

//import PropagateLoader from 'react-spinners/PropagateLoader'
import {useState, useEffect, useContext} from 'react';


import {Form, Button,Col,Row} from 'react-bootstrap';
export default function Products() {

	const {user, setUser} = useContext(UserContext);
	console.log(user)
/*	const [isOpen, setIsOpen] = useState(false);

      const openModal = () => {
        setIsOpen(true);
      };

      const closeModal = () => {
        setIsOpen(false);
      };*/
	// const [products, setProducts] = useState([]);
	// useEffect(() =>{
  //   	fetch(`${process.env.REACT_APP_API_URL}/products/active`)
	//         .then(res => res.json())
  //       	.then(data => {
	        
	//             setProducts(data.map(products => {
	//             	return(
	//             		<Highlights key={products.id} productProps={products}/>
	//             		)
	//             }))
	//         });
  //   }, [products])
//{/*	key={product.id} productProps={product}/*{products}/}

	const [products, setProducts] = useState([])
	const [loading, setLoading] = useState(true)

	const fetchProducts = async () => {
		setLoading(true)

		const res = await fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		const data = await res.json()

		setProducts(data.map(prod => (
			<Col key={prod._id} xs={12} md={6} lg={4}>
				<Highlights productProps={prod}/>
			</Col>
		)))

		setLoading(false)
	}

	useEffect(() => {
		fetchProducts()
	}, [])

	return(

		 <div className="p-0">
			
			<section className="shopping-list" id="products">
					<div className="p-5">
						<h1 className="mb-4 text-center p-0 -m-0">Our Best Products</h1>
						<Row>
							{ loading ? (
									<div className="loader">							
										{/*//<PropagateLoader color="#8f7840" />*/}
									</div>
								) : (
									products
								)
							}
						</Row>
					</div>
				</section>
		</div>

		)
}