import './App.css';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar.js'
import {Container} from 'react-bootstrap'
import Home from './pages/Home.js'
import Login from './pages/Login.js'
import Register from './pages/Register.js'
import Logout from './pages/Logout.js'
import AdminPage from './pages/AdminPage.js'
import AddProduct from './pages/AddProduct.js'
import Profile from './pages/UserProfile.js'
import {UserProvider} from './UserContext.js';
import ProductView from './components/home/ProductView.js'
import PageNotFound from './pages/PageNotFound.js';
import Products from './pages/Products.js'
import AdminUserSettings from './pages/AdminUserSettings.js'

import {user} from './pages/Login.js'
import {useState, useEffect,useContext} from 'react'
function App() {

  
  const [user, setUser] = useState({

    token:localStorage.getItem('token'),
    id:localStorage.getItem('id'),
    isAdmin:localStorage.getItem('isAdmin')
  
  })
  
  const unsetUser = () =>{
   
    localStorage.clear();
  }

console.log(user)
  return (

    <UserProvider value={{user, setUser, unsetUser}}> 
         <>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
              <Route exact path="/login" element={<Login/>}/>
              <Route exact path="/register" element={<Register/>}/>
              <Route exact path="/logout" element={<Logout/>}/>
              <Route exact path="/admin" element={<AdminPage/>}/>
              <Route exact path="/admin/addProduct" element={<AddProduct/>}/>
              <Route exact path="/admin/usersettings" element={<AdminUserSettings/>}/>
              <Route exact path="/user/profile" element={<Profile/>}/>
              <Route exact path="/products" element={<Products/>}/>
              <Route exact path="/product/:productId" element={<ProductView/>}/>
             


              <Route path="*" element={<PageNotFound/>} />
            </Routes>
          </Container>
        </Router>  
      </>
    </UserProvider>
  );
}

export default App;
